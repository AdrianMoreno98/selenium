const {Builder, By, Key, until} = require('selenium-webdriver');
const assert = require('assert');
const fs = require('fs');
const { TestScheduler } = require('jest');

const writeScreenshot = (data,name) => fs.writeFileSync(name,data,'base64');

test('Should produce 64 as an output after multiplying 8*8' , async() =>
{
    //Creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    //Get a page
    await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');

    await (await driver.findElement({name:'eight'})).click();
    await (await driver.findElement({name:'mul'})).click();
    await (await driver.findElement({name:'eight'})).click();
    await (await driver.findElement({name:'result'})).click();

    const result = await
    (
        await driver.findElement({name:'Display'})
    ).getAttribute('value');

    assert.strictEqual(result,'64');

    driver.takeScreenshot().then((data) => writeScreenshot(data, 'outputN1.png'))
    await driver.quit();
});

test('Should produce 25 as an output after multiplying 5*5' , async() =>
{
    //Creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    //Get a page
    await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');

    await (await driver.findElement({name:'five'})).click();
    await (await driver.findElement({name:'mul'})).click();
    await (await driver.findElement({name:'five'})).click();
    await (await driver.findElement({name:'result'})).click();

    const result = await
    (
        await driver.findElement({name:'Display'})
    ).getAttribute('value');

    assert.strictEqual(result,'25');

    driver.takeScreenshot().then((data) => writeScreenshot(data, 'outputN2.png'))
    await driver.quit();
});

/*test('Google should has Google in title', async () => {
    // creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    // get a page
    await driver.get('https://www.google.com/');
    // check if the title contains Google (repeat that process 1 sec. till true)
    await driver.wait(until.titleContains('Google'), 1000);
    await driver.quit();
});

test('Google results should has GitLab link after searching GitLab', async () => {
    // creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    // get a page
    await driver.get('https://www.google.com/');
    // send some keys
    await driver.wait(until.elementLocated(By.name('q')), 1000);
    await (await driver.findElement({ name: 'q' })).sendKeys('gitlab', Key.ENTER);
    // check if link is available
    await driver.wait(until.elementLocated(By.partialLinkText('GitLab.org')), 5000);
    await driver.quit();
});*/
